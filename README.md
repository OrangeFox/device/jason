# android_device_xiaomi_jason
For building TWRP for Xiaomi Mi Note 3 ONLY

To compile

. build/envsetup.sh && lunch omni_jason-eng && make -j$(nproc) recoveryimage

The Mi Note 3 (codenamed _"jason"_) are high-end mid-range smartphones from Xiaomi.

Xiaomi Mi Note 3 was announced and released in September 2017.

## Device specifications

| Device       | Xiaomi Mi Note 3                         |
| -----------: | :---------------------------------------------- |
| SoC          | Qualcomm Snapdragon 660                  |
| CPU          | Octa-core (4x2.2 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver)             |
| GPU          | Adreno 512                                      |
| Memory       | 4GB / 6GB RAM (LPDDR4X)                         |
| Shipped Android version | 7.1.1                                |
| Storage      | 64/128GB eMMC 5.1 flash storage                     |
| Battery      | Non-removable Li-Po 3500 mAh                    |
| Dimensions   | 152.6 x 74 x 7.6 mm (6.01 x 2.91 x 0.30 in)                          |
| Display      | 1080 x 1920 pixels, 16:9 ratio (~401 ppi density) 5.5 inch                   |
| Rear camera 1 | 12 MP, f/1.8, 27mm (wide), PDAF, 4-axis OIS |
| Rear camera 2 | 12 MP, f/2.6, 52mm (telephoto), AF, 2x optical zoom                |
| Front camera | 16 MP, f/2.2, 26mm (wide), 1/3.06", 1.0µm|

## Device picture

![Xiaomi Mi Note 3](https://wiki.lineageos.org/images/devices/jason.png)
